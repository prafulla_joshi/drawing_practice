//Headerfiles
#include<windows.h>
#include<gl/GL.h>
#include<stdio.h>		// For file I/O operation, FILE structure is needed
#include<gl/GLU.h>
#define _USE_MATH_DEFINES 1
#include<math.h>
#include<dos.h>
#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")
#define WIN_WIDTH 800
#define WIN_HEIGHT 600



// Global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// Global varibale declarations

bool bFullScreen = false;
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
HWND ghwnd = NULL;

HDC ghdc = NULL;
HGLRC ghrc = NULL;	// global handle to rendering context

bool gbActiveWindow = false;

FILE *gpFile = NULL;

// Following variables are for update functionality

//float xValue = -4.2f;
//float yValue = 3.0f;
//float zValue = -1.0f;

float xValue = 0.0f;
float yValue = 0.0f;
//float zValue = 0.0f;
float angle = M_PI;
float upperLeftAngle = M_PI;
float lowerLeftAngle = M_PI;
float xValue_Plane = -4.5f;
float yValue_Plane = 0.0f;

float rightUpperAngle = (3 * M_PI) / 2;
float rightLowerAngle = M_PI / 2;
float rightXValue = 0.0f;
float rightYValue = 0.0f;

//float xPlane = 0.0f;

float leftUpperAngle = M_PI / 2;
//float leftLowerAngle = M_PI / 2;

float xValue_Line = -4.5f;


// Winmain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hprevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Local function declarations
	int initialize(void);
	void uninitialize(void);
	void display(void);		// this is for double buffering
	void update(void);

	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log file cannot be created !!"), TEXT("Error"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log file created successfully\n");
	}

	// Local variable declarations
	WNDCLASSEX wndclass;
	TCHAR szAppName[] = TEXT("MyFullscreenWindow");
	HWND hwnd;
	MSG msg;

	bool bDone = false;
	int iRet = 0;


	// code
	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;

	// Register above class

	RegisterClassEx(&wndclass);

	// Create Window

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("FFP_Circle_Using_Points!!"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,	//Take desktop window as parent
		NULL,	// No menu
		hInstance,	// current instance of window
		NULL);	//If you want to send something to WndProc from WinMain

	ghwnd = hwnd;

	iRet = initialize();

	if (iRet == -1)
	{
		fprintf(gpFile, "ChoosePixelFormat() failed !!\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		fprintf(gpFile, "SetPixelFormat() failed !!\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf(gpFile, "wglCreateContext() failed !!\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf(gpFile, "wglmakeCurrent() failed !!\n");
		DestroyWindow(hwnd);
	}
	else
	{
		fprintf(gpFile, "Initialization successful !!!\n");
	}

	ShowWindow(hwnd, iCmdShow);
	//UpdateWindow(hwnd);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	// Game Loop

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				update();				// Here call to Update() ==> Animation
			}
			display();
			// Here call to Display(); though for this application we are calling in WM_PAINT
		}
	}

	return (int)msg.wParam;
}

// Call back function body

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// local function declarations
	void resize(int, int);
	//void display(void);		// Removed for double buffering
	void uninitialize(void);
	void ToggleFullscreen(void);
	//	Code
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;
	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
		//case WM_PAINT:
		//	display();	// This should be done only in single buffering
		//	break;
	case WM_ERASEBKGND:
		return 0;	// Don't go to DefWindowProc(), I have external painter, I don't want your painter
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			//MessageBox(hwnd, TEXT("Exiting on Escape Key !!"), TEXT("VK_ESCAPE handled"), MB_OK);
			DestroyWindow(hwnd);
			break;
		case 0x46:
			ToggleFullscreen();
			break;
		}
		break;
	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;
	}
	return DefWindowProc(hwnd, iMsg, wParam, lParam);
}


void ToggleFullscreen(void)
{
	MONITORINFO mi;

	if (bFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };

			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY
			), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE,
					dwStyle & ~WS_OVERLAPPEDWINDOW);

				SetWindowPos(ghwnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		bFullScreen = true;
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(ghwnd, &wpPrev);

		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
		bFullScreen = false;
	}
}

int initialize(void)
{
	// Function declaration
	void resize(int, int);

	// Variable declaration

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	// Code
	// Initialize pfd structure
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;	// This is openGL version supported by window.
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	ghdc = GetDC(ghwnd);
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		return -1;
	}
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return -2;
	}

	ghrc = wglCreateContext(ghdc);

	if (ghrc == NULL)
	{
		return -3;
	}
	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		return -4;
	}
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	// warm-up call to resize()
	resize(WIN_WIDTH, WIN_HEIGHT);		// for size dependent resources to be adjusted according window
	ToggleFullscreen();
	return 0;
}

void resize(int width, int height)
{
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void update()
{
	//Code
	/*if (yValue > 0.0f)
	{
		yValue = yValue - 0.01f;
	}
	fprintf(gpFile, "yValue = %f\n", yValue);
	if (yValue <= 0.0f)
	{
		
		angle = 0.0f;
		if (xValue <= 0.0f)
		{
			xValue = xValue + 0.01f;
		}
	}*/

	//for (angle = 0.0f; angle < 1.0f*M_PI; angle = angle + 0.00001f)
	
	
}



void display(void)
{
	//Function declaration
	void Draw_Circle(void);
	void Draw_Text(void);
	void Draw_Aeroplane(void);
	void Draw_A_Strip(void);
	void Draw_Tricolor();
	//Code
	glClear(GL_COLOR_BUFFER_BIT);
	//glFlush();	// Single Buffer

	// For Upper left plane which is required in Dynamic India Assignment
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-1.5, 2.0f, -2.5f);
	//glRotatef(90.0f, 0.0f, 0.0f, -1.0f);
	if (upperLeftAngle < (3.0f*(M_PI / 2.0f)))			
	{
		xValue = 2.0f * cos(upperLeftAngle);
		yValue = 2.0f * sin(upperLeftAngle);
		glTranslatef(xValue, yValue, -2.5f);
		//glVertex3f(xValue, yValue, 0.0f);
		//glRotatef(angle, 0.0f, 0.0f, -1.0f);
		
	}
	//glTranslatef(xValue, yValue, -2.5f);	// Commented to remove the plane once it is on X-axis
	Draw_Aeroplane();
	Draw_Text();
	//glVertex3f(xValue, yValue, 0.0f);
	
	
	Draw_Tricolor();
	upperLeftAngle = upperLeftAngle + 0.00031f;
	//angle = angle - 0.01f;

	/*--------Lower Plane------------*/

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-1.5f, -2.0f, -2.5f);
	if (lowerLeftAngle > (M_PI / 2.0f))
	{
		xValue = 2.0f * cos(lowerLeftAngle);
		yValue = 2.0f * sin(lowerLeftAngle);
		glTranslatef(xValue, yValue, -2.5f);
	}
	/*glTranslatef(xValue, yValue, -2.5f);*/		// Commented to remove the plane once it is on X-axis
	Draw_Aeroplane();
	Draw_Text();
	Draw_Tricolor();
	lowerLeftAngle = lowerLeftAngle - 0.00031f;		//0.0006f;

	/*--------Middle Plane------------*/

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(xValue_Plane, 0.0f, -2.5f);
	
	if (xValue_Plane <= 2.15f)		// hard-coded value, after which 3 planes separates
	{
		xValue_Plane = xValue_Plane + 0.00074f;		//0.00095f;
		//xValue_Line = xValue_Line + 0.00074f;
		/*glTranslatef(xValue_Plane, 0.0f, -2.5f);
		Draw_Aeroplane();
		Draw_Text();*/
		////xValue_Line = xValue_Line + 0.00074f;
		//Draw_Tricolor();
	}

	glTranslatef(xValue_Plane, 0.0f, -2.5f);
	Draw_Aeroplane();
	Draw_Text();
	//xValue_Line = xValue_Line + 0.00074f;
	Draw_Tricolor();

	//if (xValue_Line <= 1.15f)				// Keep the tricolor after plane gone
	//{
	//	xValue_Line = xValue_Line + 0.00074f;
	//	glTranslatef(xValue_Line, 0.0f, -2.5f);		// sustain tricolor; therefore it is given after if block
	//	Draw_Tricolor();
	//	
	//}
	


	if (xValue_Plane > 1.15f)
	{
		Draw_A_Strip();
	}

	/*---- Upper Right Plane ----*/
	if (xValue_Plane > 1.15f)		// 1.15 is the value of X where plane separates,hardcoded value after many trials
	{
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(2.3f, 1.6f, -2.5f);		// here values of X and Y are center point of that arc
		
		if (rightUpperAngle <= (2.0f*M_PI)+0.5f)
		{
			rightXValue = 1.5f * cos(rightUpperAngle);
			rightYValue = 1.5f * sin(rightUpperAngle);
			glTranslatef(rightXValue, rightYValue, -2.5f);
			Draw_Aeroplane();
			Draw_Text();
			rightUpperAngle = rightUpperAngle + 0.0009f;
		}
	}

	/*---- Lower Right Plane ----*/
	
	if (xValue_Plane > 1.15f)		// 1.15 is the value of X where plane separates,hardcoded value after many trials
	{
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(2.3f, -1.6f, -2.5f);		// here values of X and Y are center point of that arc
	
		if (rightLowerAngle >= 0.0f)
		{
			rightXValue = 1.5f * cos(rightLowerAngle);
			rightYValue = 1.5f * sin(rightLowerAngle);
			glTranslatef(rightXValue, rightYValue, -2.5f);
			Draw_Aeroplane();
			Draw_Text();
			rightLowerAngle = rightLowerAngle - 0.0009f;
		}
	}



	SwapBuffers(ghdc);
}


void Draw_A_Strip()
{
	//Code
	//Following code is for tri-color strip in 'A'


	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.7f, 0.014f, -2.5f);
	glLineWidth(5.0f);
	glBegin(GL_LINES);

	// Below code for saffron colorkk
	glColor3f(1.0f, 0.6f, 0.2f);							// Saffron color, RGB values taken from wiki 
	glVertex3f(-0.123f, 0.012f, 0.0f);

	glColor3f(1.0f, 0.6f, 0.2f);							// Saffron color, RGB values taken from wiki 
	glVertex3f(0.123f, 0.012f, 0.0f);
	//saffron color ends here

	// Below code for white color
	glColor3f(1.0f, 1.0f, 1.0f);							// White color, RGB values taken from wiki 
	glVertex3f(-0.123f, 0.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);							// White color, RGB values taken from wiki 
	glVertex3f(0.123f, 0.0f, 0.0f);
	//white color ends here

	// Below code for green color
	glColor3f(0.0705882f, 0.5333333f, 0.02745098f);			// Green color, RGB values taken from wiki 
	glVertex3f(-0.123f, -0.012f, 0.0f);

	glColor3f(0.0705882f, 0.5333333f, 0.02745098f);			// Green color, RGB values taken from wiki 
	glVertex3f(0.123f, -0.012f, 0.0f);
	//green color ends here

	glEnd();
}


void Draw_Aeroplane()
{

	//Code

	// Middle rectangle of Aeroplane which is required in Dynamic India Assignment
	
	//glRotatef(angle, 0.0f, 0.0f, zValue);

	glBegin(GL_QUADS);
	glColor3f(0.7294117f, 0.8862745f, 0.9333333f);
	glVertex3f(0.45f, 0.18f, 0.0f);

	glColor3f(0.7294117f, 0.8862745f, 0.9333333f);
	glVertex3f(-0.45f, 0.18f, 0.0f);

	glColor3f(0.7294117f, 0.8862745f, 0.9333333f);
	glVertex3f(-0.45f, -0.18f, 0.0f);

	glColor3f(0.7294117f, 0.8862745f, 0.9333333f);
	glVertex3f(0.45f, -0.18f, 0.0f);
	glEnd();
	
	
	// Front Triangle of Aeroplane which is required in Dynamic India Assignment
	
	glBegin(GL_TRIANGLES);
	glColor3f(0.7294117f, 0.8862745f, 0.9333333f);
	glVertex3f(0.45f, 0.18f, 0.0f);

	glColor3f(0.7294117f, 0.8862745f, 0.9333333f);
	glVertex3f(0.45f, -0.18f, 0.0f);

	glColor3f(0.7294117f, 0.8862745f, 0.9333333f);
	glVertex3f(0.8f, -0.18f, 0.0f);

	glEnd();


	// Upper Triangle of Aeroplane which is required in Dynamic India Assignment
	
	glBegin(GL_TRIANGLES);

	glColor3f(0.7294117f, 0.8862745f, 0.9333333f);
	glVertex3f(-0.45f, 0.18f, 0.0f);

	glColor3f(0.7294117f, 0.8862745f, 0.9333333f);
	glVertex3f(-0.20f, 0.18f, 0.0f);

	glColor3f(0.7294117f, 0.8862745f, 0.9333333f);
	glVertex3f(-0.45f, 0.45f, 0.0f);

	glEnd();

	// Side Triangle of Aeroplane which is required in Dynamic India Assignment
	
	glBegin(GL_TRIANGLES);

	glColor3f(0.7294117f, 0.8862745f, 0.9333333f);
	glVertex3f(-0.25f, -0.13f, 0.0f);

	glColor3f(0.7294117f, 0.8862745f, 0.9333333f);
	glVertex3f(-0.25f, -0.40f, 0.0f);

	glColor3f(0.7294117f, 0.8862745f, 0.9333333f);
	glVertex3f(0.45f, -0.13f, 0.0f);

	glEnd();

}

/*---------------------AEROPLANE DRAWING ENDS HERE------------------------*/


void Draw_Tricolor()
{
	//Code

	/*----------Tricolor behind plane starts here-----------*/

	glBegin(GL_LINES);
	//Saffron line
	glColor3f(1.0f, 0.6f, 0.2f);		// Saffron color, RGBs taken from wikipedia
	glVertex3f(-0.45f, 0.05f, 0.0f);

	glColor3f(1.0f, 0.6f, 0.2f);		// Saffron color, RGBs taken from wikipedia
	glVertex3f(-6.0f, 0.05f, 0.0f);

	//White line
	glColor3f(1.0f, 1.0f, 1.0f);		// White color, RGBs taken from wikipedia
	glVertex3f(-0.45f, 0.03f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);		// White color, RGBs taken from wikipedia
	glVertex3f(-6.0f, 0.03f, 0.0f);

	//Green line
	glColor3f(0.0705882f, 0.5333333f, 0.02745098f);		// Green color, RGBs taken from wikipedia
	glVertex3f(-0.45f, 0.0004f, 0.0f);

	glColor3f(0.0705882f, 0.5333333f, 0.02745098f);		// Green color, RGBs taken from wikipedia
	glVertex3f(-6.0f, 0.0004f, 0.0f);

	glEnd();

	/*----------Tricolor behind plane ends here-----------*/

}

void Draw_Text()
{
	glLineWidth(5.0f);
	glBegin(GL_LINES);
	// For 'I' in IAF
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-0.2f, 0.15f,0.0f);

	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-0.2f, -0.15f, 0.0f);
	
	// For 'A' in IAF
	//Left line of 'A'
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.15f, 0.0f);

	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-0.12f, -0.15f, 0.0f);
	//Right line of 'A'
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.15f, 0.0f);

	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.12f, -0.15f, 0.0f);

	//Middle strip of 'A'
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-0.07f, 0.0f, 0.0f);

	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.07f, 0.0f, 0.0f);

	// For 'F' in IAF // vertical line of F
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.2f, 0.15f, 0.0f);

	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.2f, -0.15f, 0.0f);

	//Upper Line of 'F'
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.2f, 0.15f, 0.0f);

	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.35f, 0.15f, 0.0f);

	//Lower Line of 'F'
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.2f, 0.02f, 0.0f);

	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.3f, 0.02f, 0.0f);


	glEnd();
}


void Draw_Circle(void)
{
	for (float angle = 0.0f; angle < 1.0f*M_PI; angle = angle + 0.00001f)
	{
		glVertex3f(cos(angle), sin(angle), 0.0f);
	}
}


void uninitialize(void)
{
	if (bFullScreen == true)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
		//bFullScreen = false;
	}

	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gpFile)
	{
		fprintf(gpFile, "Log file closed successfully !!\n");
		fclose(gpFile);
		gpFile = NULL;
	}
}
