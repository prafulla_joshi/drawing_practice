//Headerfiles
#include<windows.h>
#include<gl/GL.h>
#include<stdio.h>		// For file I/O operation, FILE structure is needed
#include<gl/GLU.h>
#include<math.h>
#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")
#define WIN_WIDTH 800
#define WIN_HEIGHT 600


// Global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// Global varibale declarations

bool bFullScreen = false;
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
HWND ghwnd = NULL;

HDC ghdc = NULL;
HGLRC ghrc = NULL;	// global handle to rendering context

bool gbActiveWindow = false;

FILE *gpFile = NULL;
// Winmain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hprevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Local function declarations
	int initialize(void);
	void uninitialize(void);
	void display(void);		// this is for double buffering

	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log file cannot be created !!"), TEXT("Error"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log file created successfully\n");
	}

	// Local variable declarations
	WNDCLASSEX wndclass;
	TCHAR szAppName[] = TEXT("MyFullscreenWindow");
	HWND hwnd;
	MSG msg;

	bool bDone = false;
	int iRet = 0;


	// code
	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;

	// Register above class

	RegisterClassEx(&wndclass);

	// Create Window

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("FFP_Shivjayanti_Special!!"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,	//Take desktop window as parent
		NULL,	// No menu
		hInstance,	// current instance of window
		NULL);	//If you want to send something to WndProc from WinMain

	ghwnd = hwnd;

	iRet = initialize();

	if (iRet == -1)
	{
		fprintf(gpFile, "ChoosePixelFormat() failed !!\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		fprintf(gpFile, "SetPixelFormat() failed !!\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf(gpFile, "wglCreateContext() failed !!\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf(gpFile, "wglmakeCurrent() failed !!\n");
		DestroyWindow(hwnd);
	}
	else
	{
		fprintf(gpFile, "Initialization successful !!!\n");
	}

	ShowWindow(hwnd, iCmdShow);
	//UpdateWindow(hwnd);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	// Game Loop

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				// Here call to Update() ==> Animation
			}
			display();
			// Here call to Display(); though for this application we are calling in WM_PAINT
		}
	}

	return (int)msg.wParam;
}

// Call back function body

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// local function declarations
	void resize(int, int);
	//void display(void);		// Removed for double buffering
	void uninitialize(void);
	void ToggleFullscreen(void);
	//	Code
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;
	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
		//case WM_PAINT:
		//	display();	// This should be done only in single buffering
		//	break;
	case WM_ERASEBKGND:
		return 0;	// Don't go to DefWindowProc(), I have external painter, I don't want your painter
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			//MessageBox(hwnd, TEXT("Exiting on Escape Key !!"), TEXT("VK_ESCAPE handled"), MB_OK);
			DestroyWindow(hwnd);
			break;
		case 0x46:
			ToggleFullscreen();
			break;
		}
		break;
	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;
	}
	return DefWindowProc(hwnd, iMsg, wParam, lParam);
}


void ToggleFullscreen(void)
{
	MONITORINFO mi;

	if (bFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };

			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY
			), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE,
					dwStyle & ~WS_OVERLAPPEDWINDOW);

				SetWindowPos(ghwnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		bFullScreen = true;
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(ghwnd, &wpPrev);

		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
		bFullScreen = false;
	}
}

int initialize(void)
{
	// Function declaration
	void resize(int, int);

	// Variable declaration

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	// Code
	// Initialize pfd structure
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;	// This is openGL version supported by window.
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	ghdc = GetDC(ghwnd);
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		return -1;
	}
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return -2;
	}

	ghrc = wglCreateContext(ghdc);

	if (ghrc == NULL)
	{
		return -3;
	}
	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		return -4;
	}
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	// warm-up call to resize()
	resize(WIN_WIDTH, WIN_HEIGHT);		// for size dependent resources to be adjusted according window
	return 0;
}

void resize(int width, int height)
{
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}


//GLobal Variables
double PI = 3.14;
int CirclePoints = 10000;
double dAngle = 0.0f;


void display(void)
{
	void DrawRect(void);
	void DrawTriangle(void);
	void DrawLeftTree(void);
	void DrawRightTree(void);
	void DrawFlag(void);
	glClear(GL_COLOR_BUFFER_BIT);
	//glFlush();	// Single Buffer

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -1.3f);
	float i;


	// For X-Axis
	glLineWidth(1.0f);
	glBegin(GL_LINES);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex2f(-1.0f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex2f(1.0f, 0.0f);

	glEnd();


	// For Y-Axis
	glLineWidth(1.0f);
	glBegin(GL_LINES);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex2f(0.0f, 1.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex2f(0.0f, -1.0f);

	glEnd();


	DrawRect();

	DrawTriangle();

	DrawLeftTree();
	DrawRightTree();

	DrawFlag();

	SwapBuffers(ghdc);
}


void DrawRect(void)
{
	// Top Rectangle
	//Following code is for Sky


	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -1.3f);

	glBegin(GL_QUADS);

	glColor3f(0.0f, 0.749019f, 1.0f);
	glVertex3f(1.0f, 1.0f, 0.0f);

	glColor3f(0.0f, 0.749019f, 1.0f);
	glVertex3f(-1.0f, 1.0f, 0.0f);

	glColor3f(0.0f, 0.749019f, 1.0f);
	glVertex3f(-1.0f, 0.0f, 0.0f);

	glColor3f(0.0f, 0.749019f, 1.0f);
	glVertex3f(1.0f, 0.0f, 0.0f);

	glEnd();

	// Bottom Rectangle
	// Following code is for Sand
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -1.3f);
	//glRotatef(30.0f, 0.0f, 0.0f, 1.0f);
	glBegin(GL_QUADS);

	glColor3f(0.956862f, 0.643137f, 0.376470f);
	glVertex3f(1.0f, -0.1f, 0.0f);

	glColor3f(0.956862f, 0.643137f, 0.376470f);
	glVertex3f(-1.0f, -0.1f, 0.0f);

	glColor3f(0.956862f, 0.643137f, 0.376470f);
	glVertex3f(-1.0f, -1.0f, 0.0f);

	glColor3f(0.956862f, 0.643137f, 0.376470f);
	glVertex3f(1.0f, -1.0f, 0.0f);

	glEnd();

}



void DrawLeftTree(void)
{
	//Local Functions
	void DrawCircle(float);

	// Following code is for Left Tree
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.1f, -0.05f, -1.3f);

	glBegin(GL_QUADS);


	glColor3f(0.803921f, 0.521568f, 0.247058f);
	glVertex3f(-0.8f, -0.2f, 0.0f);

	glColor3f(0.803921f, 0.521568f, 0.247058f);
	glVertex3f(-0.84f, -0.2f, 0.0f);

	glColor3f(0.803921f, 0.521568f, 0.247058f);
	glVertex3f(-0.84f, -0.40f, 0.0f);

	glColor3f(0.803921f, 0.521568f, 0.247058f);
	glVertex3f(-0.8f, -0.40f, 0.0f);

	glEnd();

	// Follwoing code is for Left tree leaves
	//1st circle
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-0.80f, -0.25f, -1.3f);

	glBegin(GL_TRIANGLE_FAN);
	DrawCircle(0.08f);
	glEnd();

	// 2nd Circle
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-0.85f, -0.1355f, -1.3f);

	glBegin(GL_TRIANGLE_FAN);
	DrawCircle(0.08f);
	glEnd();

	// 3rd Circle
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-0.75f, -0.08f, -1.3f);

	glBegin(GL_TRIANGLE_FAN);
	DrawCircle(0.08f);
	glEnd();

	// 4th Circle
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-0.65f, -0.1f, -1.3f);

	glBegin(GL_TRIANGLE_FAN);
	DrawCircle(0.08f);
	glEnd();

	// 5th Circle
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-0.66f, -0.2f, -1.3f);

	glBegin(GL_TRIANGLE_FAN);
	DrawCircle(0.08f);
	glEnd();

	// 6th Circle
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-0.75f, -0.2f, -1.3f);

	glBegin(GL_TRIANGLE_FAN);
	DrawCircle(0.08f);
	glEnd();


	// 7th Circle
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-0.75f, -0.2f, -1.3f);

	glBegin(GL_TRIANGLE_FAN);
	for (int i = 0; i < CirclePoints; i++)
	{
		dAngle = 2 * PI*i / CirclePoints;
		glColor3f(1.0f, 0.270588f, 0.0f);
		glVertex2f(0.01*cos(dAngle), 0.01*sin(dAngle));
	}
	glEnd();

	// 8th Circle
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-0.66f, -0.2f, -1.3f);

	glBegin(GL_TRIANGLE_FAN);
	for (int i = 0; i < CirclePoints; i++)
	{
		dAngle = 2 * PI*i / CirclePoints;
		glColor3f(1.0f, 0.270588f, 0.0f);
		glVertex2f(0.01*cos(dAngle), 0.01*sin(dAngle));
	}
	glEnd();

	// 9th Circle
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-0.65f, -0.1f, -1.3f);

	glBegin(GL_TRIANGLE_FAN);
	for (int i = 0; i < CirclePoints; i++)
	{
		dAngle = 2 * PI*i / CirclePoints;
		glColor3f(1.0f, 0.270588f, 0.0f);
		glVertex2f(0.01*cos(dAngle), 0.01*sin(dAngle));
	}
	glEnd();

	// 10th Circle
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-0.85f, -0.1355f, -1.3f);

	glBegin(GL_TRIANGLE_FAN);
	for (int i = 0; i < CirclePoints; i++)
	{
		dAngle = 2 * PI*i / CirclePoints;
		glColor3f(1.0f, 0.270588f, 0.0f);
		glVertex2f(0.01*cos(dAngle), 0.01*sin(dAngle));
	}
	glEnd();
}


void DrawRightTree(void)
{
	//Local Functions
	void DrawCircle(float);

	// Following code is for Left Tree
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-0.1f, -0.05f, -1.3f);

	glBegin(GL_QUADS);


	glColor3f(0.803921f, 0.521568f, 0.247058f);
	glVertex3f(0.84f, -0.2f, 0.0f);

	glColor3f(0.803921f, 0.521568f, 0.247058f);
	glVertex3f(0.8f, -0.2f, 0.0f);

	glColor3f(0.803921f, 0.521568f, 0.247058f);
	glVertex3f(0.8f, -0.40f, 0.0f);

	glColor3f(0.803921f, 0.521568f, 0.247058f);
	glVertex3f(0.84f, -0.40f, 0.0f);



	glEnd();

	// Follwoing code is for Left tree leaves
	//1st circle
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.80f, -0.25f, -1.3f);

	glBegin(GL_TRIANGLE_FAN);
	DrawCircle(0.08f);
	glEnd();

	// 2nd Circle
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.85f, -0.1355f, -1.3f);

	glBegin(GL_TRIANGLE_FAN);
	DrawCircle(0.08f);
	glEnd();

	// 3rd Circle
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.75f, -0.08f, -1.3f);

	glBegin(GL_TRIANGLE_FAN);
	DrawCircle(0.08f);
	glEnd();

	// 4th Circle
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.65f, -0.1f, -1.3f);

	glBegin(GL_TRIANGLE_FAN);
	DrawCircle(0.08f);
	glEnd();

	// 5th Circle
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.66f, -0.2f, -1.3f);

	glBegin(GL_TRIANGLE_FAN);
	DrawCircle(0.08f);
	glEnd();

	// 6th Circle
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.75f, -0.2f, -1.3f);

	glBegin(GL_TRIANGLE_FAN);
	DrawCircle(0.08f);
	glEnd();


	// 7th Circle
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.75f, -0.2f, -1.3f);

	glBegin(GL_TRIANGLE_FAN);
	for (int i = 0; i < CirclePoints; i++)
	{
		dAngle = 2 * PI*i / CirclePoints;
		glColor3f(1.0f, 0.270588f, 0.0f);
		glVertex2f(0.01*cos(dAngle), 0.01*sin(dAngle));
	}
	glEnd();

	// 8th Circle
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.66f, -0.2f, -1.3f);

	glBegin(GL_TRIANGLE_FAN);
	for (int i = 0; i < CirclePoints; i++)
	{
		dAngle = 2 * PI*i / CirclePoints;
		glColor3f(1.0f, 0.270588f, 0.0f);
		glVertex2f(0.01*cos(dAngle), 0.01*sin(dAngle));
	}
	glEnd();

	// 9th Circle
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.65f, -0.1f, -1.3f);

	glBegin(GL_TRIANGLE_FAN);
	for (int i = 0; i < CirclePoints; i++)
	{
		dAngle = 2 * PI*i / CirclePoints;
		glColor3f(1.0f, 0.270588f, 0.0f);
		glVertex2f(0.01*cos(dAngle), 0.01*sin(dAngle));
	}
	glEnd();

	// 10th Circle
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.85f, -0.1355f, -1.3f);

	glBegin(GL_TRIANGLE_FAN);
	for (int i = 0; i < CirclePoints; i++)
	{
		dAngle = 2 * PI*i / CirclePoints;
		glColor3f(1.0f, 0.270588f, 0.0f);
		glVertex2f(0.01*cos(dAngle), 0.01*sin(dAngle));
	}
	glEnd();
}


void DrawCircle(float radius)
{

	//Code

	for (int i = 0; i < CirclePoints; i++)
	{
		dAngle = 2 * PI*i / CirclePoints;
		glColor3f(0.133333f, 0.545098f, 0.133333f);
		glVertex2f(radius*cos(dAngle), radius*sin(dAngle));
	}

}

void DrawTriangle(void)
{
	// Code
	// 1st Triangle

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-0.12f, 0.0f, -1.3f);
	glLineWidth(1.0f);

	glBegin(GL_TRIANGLES);

	glColor3f(0.1960f, 0.8039f, 0.1960f);
	glVertex3f(-0.5f, 0.2f, 0.0f);

	glColor3f(0.1960f, 0.8039f, 0.1960f);
	glVertex3f(-1.0f, -0.1f, 0.0f);

	glColor3f(0.1960f, 0.8039f, 0.1960f);
	glVertex3f(-0.05f, -0.1f, 0.0f);

	glEnd();

	// 2nd Triangle

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.015f, 0.0f, -1.3f);
	glLineWidth(1.0f);

	glBegin(GL_TRIANGLES);

	glColor3f(0.1960f, 0.8039f, 0.1960f);
	glVertex3f(0.0f, 0.2f, 0.0f);

	glColor3f(0.1960f, 0.8039f, 0.1960f);
	glVertex3f(-0.5f, -0.1f, 0.0f);

	glColor3f(0.1960f, 0.8039f, 0.1960f);
	glVertex3f(0.5f, -0.1f, 0.0f);

	glEnd();

	// 3rd Triangle
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.12f, 0.0f, -1.3f);
	glLineWidth(1.0f);

	glBegin(GL_TRIANGLES);

	glColor3f(0.1960f, 0.8039f, 0.1960f);
	glVertex3f(0.5f, 0.2f, 0.0f);

	glColor3f(0.1960f, 0.8039f, 0.1960f);
	glVertex3f(0.05f, -0.1f, 0.0f);

	glColor3f(0.1960f, 0.8039f, 0.1960f);
	glVertex3f(1.0f, -0.1f, 0.0f);

	glEnd();
}


void DrawFlag(void)
{
	//Static gloabl variables

	static double dAngle = 0.0f;

	// Following code is for flag
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.01f, 0.05f, -1.3f);

	glRotatef(dAngle, 0.0f, 1.0f, 0.0f);
	dAngle = dAngle + 0.8f;
	if (dAngle >= 3600.0f)					//This snippet is for continuous rotation of flag
		dAngle = 0.0f;

	glLineWidth(1.0f);

	glBegin(GL_TRIANGLES);
	//1st half flag
	glColor3f(1.0f, 0.549019f, 0.0f);
	glVertex3f(0.0f, 0.4f, 0.0f);

	glColor3f(1.0f, 0.549019f, 0.0f);
	glVertex3f(0.0f, 0.3f, 0.0f);

	glColor3f(1.0f, 0.549019f, 0.0f);
	glVertex3f(0.1f, 0.35f, 0.0f);

	// 2nd half flag
	glColor3f(1.0f, 0.549019f, 0.0f);
	glVertex3f(0.0f, 0.35f, 0.0f);

	glColor3f(1.0f, 0.549019f, 0.0f);
	glVertex3f(0.0f, 0.25f, 0.0f);

	glColor3f(1.0f, 0.549019f, 0.0f);
	glVertex3f(0.1f, 0.30f, 0.0f);
	glEnd();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.01f, 0.05f, -1.3f);
	glLineWidth(1.0f);
	glLineWidth(0.4f);
	glBegin(GL_LINES);

	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.4f, 0.0f);

	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.15f, 0.0f);

	glEnd();

}

void uninitialize(void)
{
	if (bFullScreen == true)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
		//bFullScreen = false;
	}

	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gpFile)
	{
		fprintf(gpFile, "Log file closed successfully !!\n");
		fclose(gpFile);
		gpFile = NULL;
	}
}
